# Redux Example Project
A project for getting familiar with Redux

* Followed along with [this Pluralsight course](https://app.pluralsight.com/library/courses/redux-fundamentals/table-of-contents)
* Adapted to use Typescript
* Setup to use React and work in a browser (Webpack for commonjs modules)
* Overcomplicated some things intentionally (state is overcomplicated to show how to combine reducers, etc.)

## CI
Jenkins is being used for continuous integration for the Redux project. It is also using a Dockerfile to run the
pipeline steps inside of the resulting image/container.

First builds on new branches will take longer as the Jenkins workspace for that branch is new. However, subsequent runs
are much quicker since Jenkins mounts the workspace for the branch as part of the Docker container. The Git checkout and
NPM installs will be much quicker as they are already available in the workspace which already has the previous run's
checkout and install.

## Getting Started
Get everything set up by running:
```bash
$> npm install
```

then start the application with:
```bash
$> npm start
```

Once started, navigate to `localhost:3000` to see the application running. In `src/server` an express server is being
setup, which, uses the project root as its document root. This means that the express server will serve `index.html` as
the default page at `localhost:3000`. Additionally, `css/style.css` and `build/bundle.js` will be available as part of
express serving the project root as its document root.

## Redux
Redux uses the concept of a `store` as a single source of truth for an application's entire state. The store is
manipulated with `reducers`, which are pure functions, that create a new state based on the provided `action`. Actions
are simple objects that include a `type` and a `payload` ("payload" name can be anything like data, payload, etc.).

example actions:
```json
{
    type: "SOME_ACTION",
    payload: { newValue: 123 }
}
```
\-- or --
```json
{
    type: "SOME_OTHER_ACTION",
    data: "new string"
}
```

The actions are run through the reducers when `store.dispatch` is called with the action to execute. The store is
updated, and then anything that has created a `store.subscribe` will be notified of a change to the store.
