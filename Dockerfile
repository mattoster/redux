# Build an image base on Ubuntu 18.04 with node and npm
FROM www.matt-oster.com:18443/ubuntu:18.04

# Update and add sudo
RUN apt-get update && apt-get install -y sudo curl
# Add node 8.10 and npm 3.5.2
RUN apt-get install -y nodejs npm

# Setup jenkins user
ENV HOME=/var/lib/jenkins
RUN mkdir $HOME
RUN useradd --create-home --shell /bin/bash jenkins && usermod -aG sudo jenkins

# Map the UID and GID from the Jenkins node to the container user
RUN usermod -u 987 jenkins && groupmod -g 982 jenkins && chown -R jenkins:jenkins $HOME

USER jenkins
WORKDIR /var/lib/jenkins
