import { UpdateConversionResponse, UpdateFeesResponse } from "../../api";

// start re-working in typescript, gave up. would need to rework whole server stuff and figure out how to have webpack
// ignore src/server, have typescript compile it in place. just not worth it.

const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req: any, res: any, next: any) => {
    res.render('index', {});
});

/**
 * JSON Routes
 */
router.get('/api/conversion', (req: any, res: any, next: any) => {
    let originAmount = req.query.originAmount;
    const originCurrency = req.query.originCurrency;
    let destAmount = req.query.destAmount;
    const destCurrency = req.query.destCurrency;
    const calcOriginAmount = req.query.calcOriginAmount === 'true';
    const xRate = getXRate(originCurrency, destCurrency);

    // decide whether to convert TO or FROM originAmount
    if (calcOriginAmount) {
        originAmount = (parseFloat(destAmount || 10) / xRate).toFixed(2);
    } else {
        destAmount = (parseFloat(originAmount || 10) * xRate).toFixed(2);
    }

    // random timeout to simulate api response times
    setTimeout(() => {
        // res.header("Access-Control-Allow-Credentials", true);
        res.header("Access-Control-Allow-Origin", "http://localhost:63342");
        const resp: UpdateConversionResponse = {
            originAmount: originAmount,
            destAmount: destAmount,
            destCurrency: destCurrency,
            conversionRate: xRate
        };
        res.json(resp);
    }, getRandomResponseTime());
});

router.get('/api/fees', (req: any, res: any, next: any) => {
    const originAmount = req.query.originAmount;
    const originCurrency = req.query.originCurrency;
    const destCurrency = req.query.destCurrency;

    const feeAmount = getFee(originAmount, originCurrency, destCurrency) || 0;

    // random timeout to simulate api response times
    setTimeout(() => {
        res.header("Access-Control-Allow-Origin", "http://localhost:63342");
        const resp: UpdateFeesResponse = {
            originAmount: originAmount,
            originCurrency: originCurrency,
            destCurrency: destCurrency,
            feeAmount: feeAmount
        };
        res.json(resp);
    }, getRandomResponseTime());

});

/**
 * Helper functions
 */

function getXRate(originCurrency: any, destCurrency: any) {
    let rate = 1;

    // if both currencies are the same, exchange rate will be 1.
    if (originCurrency === destCurrency) {
        return rate;
    }

    rate = xRates[originCurrency + '_' + destCurrency];
    if (!rate) {
        // tslint:disable-next-line
        console.log('ERROR: Exchange rate missing for ' + originCurrency + ' -> ' + destCurrency);
    }

    return rate;
}

// Returns fee amount (feePercentage of originAmount for transaction)
function getFee(originAmount: any, originCurrency: any, destCurrency: any) {
    let feePerc: any = 2;

    feePerc = fees[originCurrency + '_' + destCurrency];

    if (!feePerc) {
        // tslint:disable-next-line
        return console.log('ERROR: Fee % missing for ' + originCurrency + ' -> ' + destCurrency);
    }

    return originAmount * feePerc / 100;

}

function getRandomResponseTime() {
    const max = 300; // ms
    const min = 30;
    return Math.floor(Math.random() * (max - min)) + min;
}

// TODO: get some real values here
// bogus values...
const xRates: any = {
    USD_EUR: 0.94,
    EUR_USD: 1 / 1.5,

    USD_JPY: 108.81,
    JPY_USD: 1 / 108.81,

    EUR_JPY: 123.79,
    JPY_EUR: 1 / 123.79
};

// in percentages
const fees: any = {
    USD_USD: 2,
    USD_EUR: 15,
    USD_JPY: 105,
    EUR_USD: 2,
    EUR_JPY: 70,
    EUR_EUR: 5,
    JPY_JPY: 2,
    JPY_USD: 26,
    JPY_EUR: 14
};

module.exports = router;
