export const currencyServer: string = 'http://localhost:3000';

export interface UpdateConversionData {
    currentlyEditing?: string;
    newValue?: string;
}

export interface UpdateConversionRequest {
    originAmount: string;
    destAmount: string;
    originCurrency: string;
    destCurrency: string;
    calcOriginAmount: boolean;
}

export interface UpdateConversionResponse {
    originAmount: string;
    destAmount: string;
    destCurrency: string;
    conversionRate: number;
}

export interface UpdateFeesData {
    originAmount: string;
}

export interface UpdateFeesRequest {
    originAmount: string;
    originCurrency: string;
    destCurrency: string;
}

export interface UpdateFeesResponse {
    originAmount: string,
    originCurrency: string,
    destCurrency: string,
    feeAmount: number
}
