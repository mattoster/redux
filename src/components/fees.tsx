import * as React from "react";

export interface FeeProps {
    conversionRate: number;
    fee: number;
    total: number;
    originCurrency: string;
    destinationCurrency: string;
}

/**
 * This component receives its values strictly from its parent and is not connected to Redux. In Redux terms, it is
 * just a plain old, boring, React component.
 */
export class FeesTable extends React.Component<FeeProps, {}> {
    public render() {
        const { conversionRate, fee, total, originCurrency, destinationCurrency } = this.props;
        const fixedFee: string = fee.toFixed(2);
        const fixedTotal: string = total.toFixed(2);
        const fixedConversion: string = conversionRate.toFixed(2);

        return (
            <div>
                <table>
                    <tbody>
                    <tr>
                        <td>Exchange Rate</td>
                        <td>1.00 {originCurrency} => {fixedConversion} {destinationCurrency}</td>
                    </tr>
                    <tr>
                        <td>Fee</td>
                        <td>{fixedFee} {originCurrency}</td>
                    </tr>
                    <tr>
                        <td className="total-label">Total Cost</td>
                        <td>{fixedTotal} {originCurrency}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}
