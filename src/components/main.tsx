import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { store } from "../store";
import Conversion from "./conversion";

/**
 * Parent component of all other components. This is rendered inside of Redux's Provider component so that the Redux
 * store/state is available to this component ans all of its children.
 */
export class Main extends React.Component<{}, {}> {

    public render() {
        return (
            <div>
                <Conversion />
            </div>
        );
    }

}

/** Render Main inside Provider to allow access to Redux */
ReactDOM.render(<Provider store={store}><Main /></Provider>, document.getElementById('container'));
