import { AxiosResponse } from 'axios';
import * as React from 'react';
import { connect, DispatchProp } from 'react-redux';
import { UpdateConversionData, UpdateConversionRequest } from "../api";
import { AmountActionType, ApplicationState, ChangeAmountAction, ConversionActions, ErrorAction } from '../store';
import { FeesTable } from './fees';

export interface ConversionState {
    originCurrency: string,
    destinationCurrency: string
    errorMsg?: string
}

// props expected to come from the component's parent, in this case, none
export interface OwnConversionProps {

}

// props expected to come from Redux state via Provider
export interface ReduxConversionProps {
    originAmount: string,
    destinationAmount: string,
    conversionRate: number,
    feeAmount: number,
    totalCost: number
}

/**
 * Combine props expected from Redux state, DispatchProp, and any external props for use by parent components. This
 * becomes the Uber props type for use by the actual component. In this case, our dispatch is expecting an action of
 * type {@link ChangeAmountAction}
 */
type ConversionProps = ReduxConversionProps & DispatchProp<ChangeAmountAction | ErrorAction> & OwnConversionProps;

/**
 * Conversion component for the amounts and currency drop-downs. Because this component is connected to Redux, it is
 * considered a "container" in Redux terms.
 */
class Conversion extends React.Component<ConversionProps, ConversionState> {
    private readonly actions: ConversionActions = new ConversionActions();
    private readonly handleOriginCurrencyChange: () => void;
    private readonly handleDestCurrencyChange: () => void;
    private originAmountInput: any;

    constructor(props: ConversionProps) {
        super(props);
        this.state = {
            originCurrency: 'USD',
            destinationCurrency: 'EUR',
            errorMsg: ''
        };
        // bind event listeners so 'this' will be available in the handlers
        this.handleOriginAmountChange = this.handleOriginAmountChange.bind(this);
        this.handleDestAmountChange = this.handleDestAmountChange.bind(this);
        this.handleOriginCurrencyChange = this.handleCurrencyChange.bind(this, 'origin');
        this.handleDestCurrencyChange = this.handleCurrencyChange.bind(this, 'destination');
        this.handleAjaxFailure = this.handleAjaxFailure.bind(this);
    }

    public componentDidMount() {
        this.originAmountInput.focus();
    }

    // we'll handle all failures the same
    public handleAjaxFailure(resp: AxiosResponse) {
        let msg: string = 'Error. Please try again later.';
        if (resp && resp.status === 0) {
            msg = 'Oh no! App appears to be offline.';
        }
        this.setState({ errorMsg: msg });
    }

    // on success ensure no error message
    public clearErrorMessage() {
        if (this.state.errorMsg) {
            this.setState({ errorMsg: '' });
        }
    }

    public handleCurrencyChange(currentlyEditing: string, event: any) {
        // TODO: refactor currency into Redux state
        const newState: ConversionState = this.state;
        if (currentlyEditing === 'origin') {
            newState.originCurrency = event.target.value;
        } else {
            newState.destinationCurrency = event.target.value;
        }
        // just change both...we have to use the callback so `this.state` will reflect the proper values when they are
        // called in _makeConversionAjaxCall()
        this.setState(newState, () => {
            // get new dest amount & conversion rates
            this.convert({});
        });
    }

    public handleOriginAmountChange(event: any) {
        const newAmount: string = event.target.value.replace(',', ''); // remove disallowed chars with replace
        this.props.dispatch({
            type: AmountActionType.CHANGE_ORIGIN_AMOUNT,
            payload: { amount: newAmount }
        });
        this.convert({ currentlyEditing: 'origin', newValue: newAmount });
    }

    public handleDestAmountChange(event: any) {
        const newAmount: string = event.target.value.replace(',', ''); // remove disallowed chars with replace
        this.props.dispatch({
            type: AmountActionType.CHANGE_DEST_AMOUNT,
            payload: { amount: newAmount }
        });
        this.convert({ currentlyEditing: 'dest', newValue: newAmount });
    }

    public convert(data: UpdateConversionData) {
        const reqParams: UpdateConversionRequest = {
            originAmount: data.newValue || this.props.originAmount,
            destAmount: data.newValue || this.props.destinationAmount,
            originCurrency: this.state.originCurrency,
            destCurrency: this.state.destinationCurrency,
            calcOriginAmount: false
        };

        // determine whether we need to calc origin or dest amount
        if (data.currentlyEditing === 'dest') {
            reqParams.calcOriginAmount = true;
        }
        this.actions.fetchConversionRate(this.props.dispatch, reqParams);
    }

    public render() {
        let errorMsg: JSX.Element = <div></div>;
        if (this.state.errorMsg) {
            errorMsg = <div className="errorMsg">{this.state.errorMsg}</div>;
        }
        const { originCurrency, destinationCurrency } = this.state;
        const { originAmount, destinationAmount, conversionRate, feeAmount, totalCost } = this.props;
        return (
            <div>
                {errorMsg}
                <label>Convert</label>&nbsp;
                <input className="amount-field" ref={(input) => this.originAmountInput = input}
                       onChange={this.handleOriginAmountChange} value={originAmount}/>
                &nbsp;
                <select value={this.state.originCurrency} onChange={this.handleOriginCurrencyChange}>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                    <option value="JPY">JPY</option>
                </select>
                &nbsp;to&nbsp;
                <input className="amount-field" onChange={this.handleDestAmountChange}
                       value={destinationAmount}/>
                &nbsp;
                <select value={this.state.destinationCurrency} onChange={this.handleDestCurrencyChange}>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                    <option value="JPY">JPY</option>
                </select>
                <br/><br/><br/>
                <FeesTable originCurrency={originCurrency} destinationCurrency={destinationCurrency}
                           conversionRate={conversionRate} fee={feeAmount} total={totalCost}/>
            </div>
        );
    }

}

/**
 * This maps Redux store/state values to the component's props, making them available to the component.
 *
 * @param {ApplicationState} state
 * @param {OwnConversionProps} props
 *
 * @returns {ReduxConversionProps}
 */
const mapStateToProps = (state: ApplicationState, props: OwnConversionProps): ReduxConversionProps => {
    return {
        originAmount: state.amountState.originAmount,
        destinationAmount: state.amountState.destinationAmount,
        conversionRate: state.amountState.conversionRate,
        feeAmount: state.amountState.feesAmount,
        totalCost: state.amountState.totalAmount
    };
};

/**
 * The connect function is what associates the Redux bits with this component as defined by mapStateToProps. This is
 * effectively the "subscribe".
 */
export default connect<ReduxConversionProps, DispatchProp<ChangeAmountAction>, OwnConversionProps, ApplicationState>(
    mapStateToProps
)(Conversion);
