export * from "./actions";
export * from "./reducers";
export * from "./configureStore";

/**
 * Top-level application state. Intentionally over-complicated to work with multiple reducers.
 *
 * Every field in this interface can have its own reducer, and in this case, amountState and currencyState each have
 * their own reducers. See their creation in reducers.ts and the way they are combined into a single reducer in
 * configureStore.ts.
 */
export interface ApplicationState {
    amountState: AmountState;
    currencyState: CurrencyState;
    errorState: ErrorState;
}

export interface AmountState {
    originAmount: string;
    destinationAmount: string;
    conversionRate: number;
    feesAmount: number;
    totalAmount: number;
}

export interface CurrencyState {
    originCurrency: string;
    destinationCurrency: string;
}

export interface ErrorState {
    err: string;
}
