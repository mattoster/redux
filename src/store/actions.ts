import Axios, { AxiosResponse } from "axios";
import { debounce } from 'lodash';
import { Action, Dispatch } from "redux";
import {
    currencyServer, UpdateConversionRequest, UpdateConversionResponse, UpdateFeesRequest, UpdateFeesResponse
} from "../api";

export interface AmountPayload {
    amount: string;
}

export interface ConversionPayload {
    originAmount: string;
    destAmount: string;
    conversionRate: number;
}

export interface FeesPayload {
    originAmount: string;
    feesAmount: number;
}

/**
 * All action types available for amountState actions
 */
export enum AmountActionType {
    CHANGE_ORIGIN_AMOUNT = "CHANGE_ORIGIN_AMOUNT",
    CHANGE_DEST_AMOUNT = "CHANGE_DEST_AMOUNT",
    UPDATE_CONVERSION_RATE = "UPDATE_CONVERSION_RATE",
    UPDATE_FEES_RATE = "UPDATE_FEES_RATE"
}

/**
 * The ChangeAmountAction defines the expected value and its type for the payload/data portion of the Redux action. It
 * also specifies that the action type is AmountActionType.
 */
export interface ChangeAmountAction extends Action<AmountActionType> {
    payload: AmountPayload | ConversionPayload | FeesPayload;
}

/**
 * All action types available for currency actions
 */
export enum CurrencyActionType {
    CHANGE_ORIGIN_CURRENCY = "CHANGE_ORIGIN_CURRENCY",
    CHANGE_DEST_CURRENCY = "CHANGE_DEST_CURRENCY"
}

/**
 * The ChangeCurrencyAction defines the expected value and its type for the payload/data portion of the Redux action. It
 * also specifies that the action type is CurrencyActionType.
 */
export interface ChangeCurrencyAction extends Action<CurrencyActionType> {
    currency: string;
}

/**
 * All action types available for error actions
 */
export enum ErrorActionType {
    ERROR_OCCURRED = "ERROR_OCCURRED"
}

/**
 * The ErrorAction defines the expected value and its type for the payload/data portion of the Redux action. It also
 * specifies that the action type is ErrorActionType.
 */
export interface ErrorAction extends Action<ErrorActionType> {
    error: string;
}

export class ConversionActions {
    private readonly makeConversionAjaxCall: (dispatch: Dispatch<ChangeAmountAction | ErrorAction>,
                                              reqParams: UpdateConversionRequest) => void;

    constructor() {
        // Add a debounced version of _getDestinationAmount() so we avoid server & UI Thrashing.
        // See http://stackoverflow.com/questions/23123138/perform-debounce-in-react-js/28046731#28046731
        this.makeConversionAjaxCall = debounce(this._makeConversionAjaxCall, 500);
    }

    /**
     * This function performs the calls to the rate conversion API through a debounced function. It then follows on
     * success by calling to get updated fees as well.
     *
     * @param {Dispatch<ChangeAmountAction | ErrorAction>} dispatch
     * @param {UpdateConversionRequest} reqParams
     */
    public fetchConversionRate = (dispatch: Dispatch<ChangeAmountAction | ErrorAction>,
                                  reqParams: UpdateConversionRequest) => {
        this.makeConversionAjaxCall(dispatch, reqParams);
    }

    /**
     * This function performs the calls to the API to get updated conversion data. It then follows on success by calling
     * to get updated fees as well.
     *
     * @param {Dispatch<ChangeAmountAction | ErrorAction>} dispatch
     * @param {UpdateConversionRequest} reqParams
     * @private
     */
    private _makeConversionAjaxCall(dispatch: Dispatch<ChangeAmountAction | ErrorAction>,
                                    reqParams: UpdateConversionRequest) {
        // ajax call for destination amount originCurrency, destCurrency, originAmount
        Axios.get(currencyServer + '/api/conversion', { params: reqParams })
             .then((resp: AxiosResponse) => {
                 const response: UpdateConversionResponse = resp.data as UpdateConversionResponse;
                 const payload: ConversionPayload = {
                     originAmount: response.originAmount,
                     destAmount: response.destAmount,
                     conversionRate: response.conversionRate
                 };
                 dispatch({
                     type: AmountActionType.UPDATE_CONVERSION_RATE,
                     payload: payload
                 });
                 // get the new fee & total amount by building request and sending it
                 const feeReqParams: UpdateFeesRequest = {
                     originAmount: reqParams.originAmount,
                     originCurrency: reqParams.originCurrency,
                     destCurrency: reqParams.destCurrency
                 };
                 this.fetchFees(dispatch, feeReqParams);
             })
             .catch((err) => {
                 dispatch({
                     type: ErrorActionType.ERROR_OCCURRED,
                     error: err.toString()
                 });
             });
    }

    /**
     * This function performs the call to the API for updating fees. No need to be debounced as it is a follow-on call
     * from an already debounced function.
     *
     * @param {Dispatch<ChangeAmountAction | ErrorAction>} dispatch
     * @param {UpdateFeesRequest} reqParams
     */
    private fetchFees = (dispatch: Dispatch<ChangeAmountAction | ErrorAction>, reqParams: UpdateFeesRequest) => {
        Axios.get(currencyServer + '/api/fees', { params: reqParams })
             .then((resp: AxiosResponse) => {
                 const response = resp.data as UpdateFeesResponse;
                 const payload: FeesPayload = {
                     feesAmount: response.feeAmount,
                     originAmount: response.originAmount
                 };
                 dispatch({
                     type: AmountActionType.UPDATE_FEES_RATE,
                     payload: payload
                 });
             })
             .catch((err) => {
                 dispatch({
                     type: ErrorActionType.ERROR_OCCURRED,
                     error: err.toString()
                 });
             });
    }

}
