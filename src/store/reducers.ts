import { Reducer, ReducersMapObject } from "redux";
import {
    AmountActionType, AmountPayload, ChangeAmountAction, ChangeCurrencyAction, ConversionPayload, CurrencyActionType,
    ErrorAction, ErrorActionType, FeesPayload
} from "./actions";
import { AmountState, ApplicationState, CurrencyState, ErrorState } from "./index";

export const amountReducer: Reducer<AmountState, ChangeAmountAction> = (state = {} as any,
                                                                        action: ChangeAmountAction) => {
    switch (action.type) {
        case AmountActionType.CHANGE_ORIGIN_AMOUNT:
            return {
                ...state,
                originAmount: (action.payload as AmountPayload).amount
            };
        case AmountActionType.CHANGE_DEST_AMOUNT:
            return {
                ...state,
                destinationAmount: (action.payload as AmountPayload).amount
            };
        case AmountActionType.UPDATE_CONVERSION_RATE:
            const originAmount: string = Number((action.payload as ConversionPayload).originAmount.replace(',', ''))
                .toFixed(2);
            const destAmount: string = Number((action.payload as ConversionPayload).destAmount.replace(',', ''))
                .toFixed(2);
            return {
                ...state,
                originAmount: originAmount,
                destinationAmount: destAmount,
                conversionRate: (action.payload as ConversionPayload).conversionRate
            };
        case AmountActionType.UPDATE_FEES_RATE:
            const newFee: number = (action.payload as FeesPayload).feesAmount;
            const newTotal: number = (Number((action.payload as FeesPayload).originAmount) || 10) + (newFee || 10);
            return {
                ...state,
                feesAmount: newFee,
                totalAmount: newTotal
            };
        default:
            return state;
    }
};

export const currencyReducer: Reducer<CurrencyState, ChangeCurrencyAction> = (state = {} as any,
                                                                              action: ChangeCurrencyAction) => {
    switch (action.type) {
        case CurrencyActionType.CHANGE_ORIGIN_CURRENCY:
            return {
                ...state,
                originCurrency: action.currency
            };
        case CurrencyActionType.CHANGE_DEST_CURRENCY:
            return {
                ...state,
                destinationCurrency: action.currency
            };
        default:
            return state;
    }
};

export const errorReducer: Reducer<ErrorState, ErrorAction> = (state = {} as any, action: ErrorAction) => {
    switch (action.type) {
        case ErrorActionType.ERROR_OCCURRED:
            return {
                ...state,
                err: action.error
            };
        default:
            return state;
    }
};

// Whenever an action is dispatched, Redux will update each top-level application state property using the reducer
// with the matching name. It's important that the names match exactly, and that the reducer acts on the
// corresponding ApplicationState property type.
export const myReducers: ReducersMapObject<ApplicationState, any> = {
    amountState: amountReducer,
    currencyState: currencyReducer,
    errorState: errorReducer
};
