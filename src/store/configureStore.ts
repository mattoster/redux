import { applyMiddleware, combineReducers, createStore, Reducer, Store } from "redux";
import { createLogger } from "redux-logger";
import { ApplicationState } from "./index";
import { myReducers } from "./reducers";

/**
 * The initial state of the application.
 */
const initialAppState: ApplicationState = {
    amountState: {
        originAmount: "0.00",
        destinationAmount: "0.00",
        conversionRate: 0,
        feesAmount: 0,
        totalAmount: 0
    },
    currencyState: {
        originCurrency: "USD",
        destinationCurrency: "EUR"
    },
    errorState: {
        err: ""
    }
};

const logger = createLogger({ collapsed: true });

/**
 * This configures the Redux store, setting its reducer and initial state.
 *
 * @param {ApplicationState} initialState
 *
 * @returns {Store<ApplicationState>}
 */
function configureStore(initialState: ApplicationState): Store<ApplicationState> {
    // We'll create our store with the combined reducers and the initial Redux state that we'll be passing from our
    // entry point.

    const rootReducer: Reducer<ApplicationState> = combineReducers<ApplicationState>(myReducers);

    // logger should be the last in the list of middlewares to prevent issues
    return createStore(rootReducer, initialState, applyMiddleware(logger));
}

/** export of the Redux store */
export const store: Store<ApplicationState> = configureStore(initialAppState);
